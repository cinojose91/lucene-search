package com.assignment1.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.en.EnglishAnalyzer;

import org.apache.lucene.analysis.shingle.ShingleAnalyzerWrapper;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import org.apache.lucene.misc.HighFreqTerms;
import org.apache.lucene.misc.HighFreqTerms.DocFreqComparator;
import org.apache.lucene.misc.TermStats;

import org.apache.lucene.search.IndexSearcher;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.queryparser.classic.QueryParser;

import com.assignment1.constant.DblpXmlTag;

public class Project2 {

	private String idxloc;
	
	//private static ShingleAnalyzerWrapper sfanalyzer = new ShingleAnalyzerWrapper(analyzer, 2);
	private static CharArraySet cset = new CharArraySet(50,true); 
	 static {
		    final List<String> stopWords = Arrays.asList(
		      "a", "an", "and", "are", "as", "at", "be", "but", "by",
		      "for", "if", "in", "into", "is", "it",
		      "no", "not", "of", "on", "or", "such",
		      "that", "the", "their", "then", "there", "these",
		      "they", "this", "to", "was", "will", "with","systems","based",
		      "using","time","data","model","from","fuzzy","new","models",
		      "method","web","system","design"
		    );
		    final CharArraySet stopSet = new CharArraySet(stopWords, false);
		    cset = CharArraySet.unmodifiableSet(stopSet); 
		  }
	private static StandardAnalyzer analyzer = new StandardAnalyzer(cset);
	private static EnglishAnalyzer es = new EnglishAnalyzer();
	private IndexWriter writer;
	private String indexDir = "index_popular";
	Path path_sub = Paths.get(indexDir);
	private int LIMIT = 1000000;

	public Project2() {
		
		
		showMenu();

	}

	public void subTopic1(String year) {
		doInitialize();
		Path path = Paths.get(idxloc);
		Directory directory;
		try {
			directory = FSDirectory.open(path);
			IndexReader indexReader = DirectoryReader.open(directory);
			IndexSearcher indexSearcher = new IndexSearcher(indexReader);
			Query query;
			
			try {
				query = new QueryParser(DblpXmlTag.PUBYEAR, es).parse(year);
				TopDocs topDocs = indexSearcher.search(query, LIMIT);
				for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
					Document document = indexSearcher.doc(scoreDoc.doc);
					writer.addDocument(document);
				}
				// int newNumDocs = writer.numDocs();
				// System.out.println(newNumDocs);
				writer.close();
				
				// Starting to look into the sub index
				DocFreqComparator cmp = new HighFreqTerms.DocFreqComparator();
				Directory directory_sub = FSDirectory.open(path_sub);
				IndexReader indexReader_sub = DirectoryReader.open(directory_sub);
				TermStats[] highFreqTerms = HighFreqTerms.getHighFreqTerms(indexReader_sub, 100, DblpXmlTag.TITLE, cmp);
				ArrayList<String> terms = new ArrayList<String>();
				for (TermStats ts : highFreqTerms) {
					terms.add(ts.termtext.utf8ToString());
				}
				System.out.println("List of poular Strings are :" + terms);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	
	public String venueAndYear(){
		String retval = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the venue followed by year(eg : CIKM 2014)");
		try {
			retval = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retval;
		
	}
	
	public void clearIndex(File folder) {
		final File[] files = folder.listFiles();
		for (File f : files)
			f.delete();
	}

	public void doInitialize() {

		clearIndex(new File(indexDir));
		FSDirectory dir;
		try {
			dir = FSDirectory.open(Paths.get(indexDir));
			IndexWriterConfig config = new IndexWriterConfig(analyzer);
			writer = new IndexWriter(dir, config);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	public void showMenu() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = "";
		String pubyear = "";
		do {
			
			System.out.println("#####Project 2 Menu #######");
			System.out.println("[1] Show popular titles keywords based on year");
			System.out.println("[3] Set index location");
			System.out.println("[4] Exit");
			try {
				s = br.readLine();
				switch (Integer.parseInt(s)) {
				case 1: {
					if(!idxloc.equals("")){
						System.out.println("Pubyear(eg :2000)");
						pubyear = br.readLine();
						subTopic1(pubyear);
					}else{
						System.out.println("Please set index location to proceed");
					}
					break;
				}
				case 2: {
					if(!idxloc.equals("")){
						//subTopic2();
					}else{
						System.out.println("Please set index location to proceed");
					}
					break;
				}
				case 3:{
					System.out.println("indexLoc :");
					try {
						idxloc = br.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while (Integer.parseInt(s) != 4);
	}
}
