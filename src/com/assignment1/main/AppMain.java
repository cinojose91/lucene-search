package com.assignment1.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.assignment1.application.Project2;
import com.assignment1.lucene.XmlFileIndexer;

public class AppMain {
	public static void main(String[] args) throws IOException {

		System.out.println("Welcome to Assignment , Project 1");
		showMenu();
		String indexLoc = "";
		XmlFileIndexer xIndexer = new XmlFileIndexer();
		int option = 0;
		
		while (option != 5) {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String s = br.readLine();
			option = Integer.parseInt(s);
			switch (option) {
			case 1:
				xIndexer.createIndex();
				break;
			case 3:
			{
				if("".equals(indexLoc)){
					System.out.println("Enter the index location: ");
					indexLoc = br.readLine();
				}
				System.out.println("Enter the number of rows to return: ");
				s = br.readLine();
				int number = Integer.parseInt(s);
				xIndexer.doSearch(indexLoc,number);
				break;
			}
			case 5:
				System.out.println("Thank you for using application");
				System.exit(0);
				break;
			default:
				System.out.println("Invalid Option");
				break;

			}
			showMenu();
		}

	}

	/*
	 * Function to print the menu
	 */
	public static void showMenu() {
		System.out.println("***********DBLP Main Menu**********");
		System.out.println("[1]. Create Index");
		System.out.println("[3]. Search");
		System.out.println("[5]. Exit");
		System.out.println("Option: ");
	}
}
