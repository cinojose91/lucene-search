package com.assignment1.lucene;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.QueryBuilder;
import org.apache.lucene.util.Version;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;

import com.assignment1.constant.DblpXmlTag;

public class XmlFileIndexer {
	private static StandardAnalyzer analyzer = new StandardAnalyzer();
	private static KeywordAnalyzer kanalyzer = new KeywordAnalyzer();
	private IndexWriter writer;
	private ArrayList<File> queue = new ArrayList<File>();
	String indexLoc = "";

	XmlFileIndexer(String indexDir) throws IOException {
		// the boolean true parameter means to create a new index everytime,
		// potentially overwriting any existing files there.
		FSDirectory dir = FSDirectory.open(Paths.get(indexDir));

		IndexWriterConfig config = new IndexWriterConfig(analyzer);

		writer = new IndexWriter(dir, config);
	}

	public XmlFileIndexer() {

	}

	public void createIndex() {
		System.out
				.println("Enter the path where the index will be created: (e.g. /tmp/indexor c:/temp/index) ");

		String indexLocation = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		XmlFileIndexer indexer = null;
		try {
			String s = br.readLine();

			indexLocation = s;
			indexer = new XmlFileIndexer(s);

			System.out
					.println("Enter the full path to add into the index (q=quit):"
							+ " (e.g. /home/ron/mydir or c:/Users/ron/mydir)");
			System.out
					.println("[Acceptable file types: .xml, .html, .html, .txt]");
			s = br.readLine();
             
			indexer.indexFileOrDirectory(s);
			indexer.closeIndex();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reIndex() {
		// TODO: create the function to do the re indexing
	}

	public void doSearch(String idxloc, int limit) {
		// TODO: create the function to do the searching.
		try {
			Path path = Paths.get(idxloc);
			Directory directory = FSDirectory.open(path);
			IndexReader indexReader = DirectoryReader.open(directory);
			IndexSearcher indexSearcher = new IndexSearcher(indexReader);
			String field = getSearchField();
			String search = getSearchQuery();
			Query query=null;
			TopDocs topDocs;
			// handling phrase queries.
			if(search.contains("\"")){
				QueryParser parser = new QueryParser("Field",analyzer);
				String [] fields = {DblpXmlTag.TITLE,DblpXmlTag.AUTHOR,DblpXmlTag.VENUE,DblpXmlTag.PUBYEAR};
				String q ="";
				for(String s : fields){
					q = q+s+":"+search+" OR ";
				}
				q = q.substring(0,(q.length()-4));
				query = parser.parse(q);
				topDocs = indexSearcher.search(query, limit);
				
			}
			else if (field.equals(DblpXmlTag.FREE_TEXT)) {
				
				query = MultiFieldQueryParser.parse(search, new String[] {
						DblpXmlTag.TITLE, DblpXmlTag.AUTHOR, DblpXmlTag.VENUE,
						DblpXmlTag.PUBYEAR },
						new BooleanClause.Occur[] { BooleanClause.Occur.SHOULD,
								BooleanClause.Occur.SHOULD,
								BooleanClause.Occur.SHOULD,
								BooleanClause.Occur.SHOULD }, analyzer);
				topDocs = indexSearcher.search(query, limit);
			} else {
				query = new QueryParser(field, analyzer).parse(search);
				topDocs = indexSearcher.search(query, limit);
			}
			//topDocs = indexSearcher.search(query, limit);
			System.out.println("totalHits " + topDocs.totalHits);
			int rank = 0;
			for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
				rank++;
				Document document = indexSearcher.doc(scoreDoc.doc);
				System.out.println("Rank :" + rank);
				System.out.println("Title :" + document.get(DblpXmlTag.TITLE));
				System.out.println("Key :" + document.get(DblpXmlTag.KEY));
				System.out.println("Author :" + document.get(DblpXmlTag.AUTHOR));
				System.out.println("Year :" + document.get(DblpXmlTag.PUBYEAR));
				System.out.println("Venue :" + document.get(DblpXmlTag.VENUE));
				System.out.println("Score :" + scoreDoc.score);
				System.out.println("------------------------------");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void indexFileOrDirectory(String fileName) throws IOException {
		// ===================================================
		// gets the list of files in a folder (if user has submitted
		// the name of a folder) or gets a single file name (is user
		// has submitted only the file name)
		// ===================================================
		addFiles(new File(fileName));
        Date datestart = new Date();
		int originalNumDocs = writer.numDocs();
		for (File f : queue) {
			try {

				// ===================================================
				// add contents of file
				// ===================================================
				SaxXmlParser sxp = new SaxXmlParser(writer);
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				saxParser.parse(f, sxp);
				System.out.println("Added: " + f);
			} catch (Exception e) {
				// System.out.print(e.getStackTrace());
				e.printStackTrace();
				System.out.println("Could not add: " + f);
			}
		}
		Date dateend = new Date();
		
		int newNumDocs = writer.numDocs();
		System.out.println("");
		System.out.println("************************");
		System.out
				.println((newNumDocs - originalNumDocs) + " documents added.");
		System.out.println("************************");
		System.out.println("Total time:"+Math.abs(datestart.getTime()-dateend.getTime())/1000+"(s)");
		queue.clear();
	}

	private void addFiles(File file) {

		if (!file.exists()) {
			System.out.println(file + " does not exist.");
		}
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				addFiles(f);
			}
		} else {
			String filename = file.getName().toLowerCase();
			// ===================================================
			// Only index text files
			// ===================================================
			if (filename.endsWith(".htm") || filename.endsWith(".html")
					|| filename.endsWith(".xml") || filename.endsWith(".txt")) {
				queue.add(file);
			} else {
				System.out.println("Skipped " + filename);
			}
		}
	}

	public void closeIndex() throws IOException {
		writer.close();
	}

	public String getSearchField() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("[1] FreeSearch on all fields");
		System.out.println("[2] Search on specific attribute");
		String s = br.readLine();
		if (Integer.parseInt(s) == 1) {
			return DblpXmlTag.FREE_TEXT;
		}
		System.out.println("[1] Title");
		System.out.println("[2] Pubyear");
		System.out.println("[3] Author");
		System.out.println("[4] Venue");
		s = br.readLine();
		switch (Integer.parseInt(s)) {
		case 1:
			return DblpXmlTag.TITLE;
		case 2:
			return DblpXmlTag.PUBYEAR;
		case 3:
			return DblpXmlTag.AUTHOR;
		case 4:
			return DblpXmlTag.VENUE;
		default:
			return "title";
		}

	}

	public String getSearchQuery() throws IOException {
		System.out.println("Enter the query (e.g., “event detection” (for phrase queries on all fields), Java.)");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		return br.readLine();
	}

}
