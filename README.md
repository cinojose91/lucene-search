# README #

This README will guide you on how to run the lucene search on the dblp xml file.

### Project Setup ###

* [Maven](http://stackoverflow.com/questions/8620127/maven-in-eclipse-step-by-step-installation) project, so eclipse need maven to download the install the application
* Once application imported to the eclipse, just run the Appmain.java and application will show the menu.

### Create Index ###

* Choose option 1 from the menu 
* Choose the location of the index to create
* Choose the xml file to parse.
* System will create an index at the location specified.

### Project 1 ###

* Choose the option 2 to Start the search from the main menu
* Choose the index location to search
* Enter the number of results.
* Choose the option to search and follow accordingly

### Project 2 part 1 ###

* Choose the option 4 from main menu to start project 2
* Choose option 1 to start show the titles based on year
* Choose the year to show the result
* Enter the year and wait for the result